# Certbot all plugins

Certbot image with all authenticator plugins already installed


## Example output

```
$ docker run registry.gitlab.com/wstucco/certbot-all-plugins

Saving debug log to /var/log/letsencrypt/letsencrypt.log

- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
* dns-cloudflare
Description: Obtain certificates using a DNS TXT record (if you are using
Cloudflare for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-cloudflare =
certbot_dns_cloudflare.dns_cloudflare:Authenticator

* dns-cloudxns
Description: Obtain certificates using a DNS TXT record (if you are using
CloudXNS for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-cloudxns = certbot_dns_cloudxns.dns_cloudxns:Authenticator

* dns-digitalocean
Description: Obtain certs using a DNS TXT record (if you are using DigitalOcean
for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-digitalocean =
certbot_dns_digitalocean.dns_digitalocean:Authenticator

* dns-dnsimple
Description: Obtain certificates using a DNS TXT record (if you are using
DNSimple for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-dnsimple = certbot_dns_dnsimple.dns_dnsimple:Authenticator

* dns-dnsmadeeasy
Description: Obtain certificates using a DNS TXT record (if you are using DNS
Made Easy for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-dnsmadeeasy =
certbot_dns_dnsmadeeasy.dns_dnsmadeeasy:Authenticator

* dns-gehirn
Description: Obtain certificates using a DNS TXT record (if you are using Gehirn
Infrastracture Service for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-gehirn = certbot_dns_gehirn.dns_gehirn:Authenticator

* dns-google
Description: Obtain certificates using a DNS TXT record (if you are using Google
Cloud DNS for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-google = certbot_dns_google.dns_google:Authenticator

* dns-linode
Description: Obtain certs using a DNS TXT record (if you are using Linode for
DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-linode = certbot_dns_linode.dns_linode:Authenticator

* dns-luadns
Description: Obtain certificates using a DNS TXT record (if you are using LuaDNS
for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-luadns = certbot_dns_luadns.dns_luadns:Authenticator

* dns-nsone
Description: Obtain certificates using a DNS TXT record (if you are using NS1
for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-nsone = certbot_dns_nsone.dns_nsone:Authenticator

* dns-ovh
Description: Obtain certificates using a DNS TXT record (if you are using OVH
for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-ovh = certbot_dns_ovh.dns_ovh:Authenticator

* dns-rfc2136
Description: Obtain certificates using a DNS TXT record (if you are using BIND
for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-rfc2136 = certbot_dns_rfc2136.dns_rfc2136:Authenticator

* dns-route53
Description: Obtain certificates using a DNS TXT record (if you are using AWS
Route53 for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-route53 = certbot_dns_route53.dns_route53:Authenticator

* dns-sakuracloud
Description: Obtain certificates using a DNS TXT record (if you are using Sakura
Cloud for DNS).
Interfaces: IAuthenticator, IPlugin
Entry point: dns-sakuracloud =
certbot_dns_sakuracloud.dns_sakuracloud:Authenticator

* nginx
Description: Nginx Web Server plugin
Interfaces: IAuthenticator, IInstaller, IPlugin
Entry point: nginx = certbot_nginx.configurator:NginxConfigurator

* standalone
Description: Spin up a temporary webserver
Interfaces: IAuthenticator, IPlugin
Entry point: standalone = certbot.plugins.standalone:Authenticator

* webroot
Description: Place files in webroot directory
Interfaces: IAuthenticator, IPlugin
Entry point: webroot = certbot.plugins.webroot:Authenticator
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
```




