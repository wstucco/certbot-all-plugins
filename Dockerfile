FROM certbot/certbot

COPY certbot/certbot-nginx src/certbot-nginx
COPY certbot/certbot-dns-cloudflare src/certbot-dns-cloudflare
COPY certbot/certbot-dns-cloudxns src/certbot-dns-cloudxns
COPY certbot/certbot-dns-digitalocean src/certbot-dns-digitalocean
COPY certbot/certbot-dns-dnsimple src/certbot-dns-dnsimple
COPY certbot/certbot-dns-dnsmadeeasy src/certbot-dns-dnsmadeeasy
COPY certbot/certbot-dns-gehirn src/certbot-dns-gehirn
COPY certbot/certbot-dns-google src/certbot-dns-google
COPY certbot/certbot-dns-linode src/certbot-dns-linode
COPY certbot/certbot-dns-luadns src/certbot-dns-luadns
COPY certbot/certbot-dns-nsone src/certbot-dns-nsone
COPY certbot/certbot-dns-ovh src/certbot-dns-ovh
COPY certbot/certbot-dns-rfc2136 src/certbot-dns-rfc2136
COPY certbot/certbot-dns-route53 src/certbot-dns-route53
COPY certbot/certbot-dns-sakuracloud src/certbot-dns-sakuracloud

RUN pip install --no-cache-dir --editable  src/certbot-nginx
RUN pip install --no-cache-dir --editable  src/certbot-dns-cloudflare
RUN pip install --no-cache-dir --editable  src/certbot-dns-cloudxns
RUN pip install --no-cache-dir --editable  src/certbot-dns-digitalocean
RUN pip install --no-cache-dir --editable  src/certbot-dns-dnsimple
RUN pip install --no-cache-dir --editable  src/certbot-dns-dnsmadeeasy
RUN pip install --no-cache-dir --editable  src/certbot-dns-gehirn
RUN pip install --no-cache-dir --editable  src/certbot-dns-google
RUN pip install --no-cache-dir --editable  src/certbot-dns-linode
RUN pip install --no-cache-dir --editable  src/certbot-dns-luadns
RUN pip install --no-cache-dir --editable  src/certbot-dns-nsone
RUN pip install --no-cache-dir --editable  src/certbot-dns-ovh
RUN pip install --no-cache-dir --editable  src/certbot-dns-rfc2136
RUN pip install --no-cache-dir --editable  src/certbot-dns-route53
RUN pip install --no-cache-dir --editable  src/certbot-dns-sakuracloud

CMD [ "plugins" ]